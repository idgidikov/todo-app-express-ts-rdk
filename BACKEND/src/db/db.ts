import mongoose from 'mongoose';

export const connectToDb = (): void => {
    const uri: string = process.env.MONGO_URI || '';

    mongoose.connect(uri, {
    
    })
        .then(() => {
            console.log('Yes, we are connected to the database!');
        })
        .catch((error) => {
            console.error('Error connecting to the database:', error);
        });
};
