import { Response, Request } from "express"
import { ObjectId } from "mongoose"
import { ITodo, Todo, TodoDoc } from "../models/todo"
interface Id{
id: ObjectId
}

export const create = async(req: Request<{},{},ITodo> , res: Response<TodoDoc>)=> {
    const {todoStatus,description} = req.body
    const newTodo = Todo.build({
        todoStatus,
        description
    })
    newTodo.save();
    return res.status(201).json(newTodo)
}

export const deleteTodo = async(
    req: Request<Id,{},ITodo>,
    res : Response
) => {
    const {id} = req.params
    await Todo.findByIdAndDelete({_id : id}).catch(err=> res.status(400).json())

    return res.status(200).json()
}

interface Error {
    error: string
}

export const updateTodo =async(
    req: Request<Id,{},ITodo>,
    res : Response
) => {
    const {id} = req.params
    
    if(!id) return res.status(400).json({
        error: "Please provide a todo id!"
    })
    const {todoStatus,description } = req.body
    try{
        const updatedTodo = await Todo.findByIdAndUpdate<TodoDoc>(
             {_id: id},
            {todoStatus, description},
            {new : true}
        )
        await updatedTodo.save();
        return res.status(200).json(updatedTodo);
    }catch(e){
        if(e.name=== "CastError")
        return res.status(400).json({error: 'Please provide a valid id!'})
    }

    return res.status(200).json()
}

export const getAllTodos = async(
    req: Request,
    res : Response<TodoDoc[]>
)=> {
    const todos = await Todo.find<TodoDoc>()
    return res.status(200).json(todos)

}

export const getTodo = async(req: Request<Id>,
    res : Response<TodoDoc>) =>{
        const {id} = req.params
        const todo = await Todo.findOne<TodoDoc>({_id: id})
        return res.status(200).json(todo)
    }

