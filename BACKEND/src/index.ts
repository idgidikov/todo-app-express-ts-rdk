import express from "express"
import dotenv from 'dotenv'
import cors from 'cors'
import { connectToDb } from "./db/db"
import { todoRoutes } from "./routes/todos"
dotenv.config()


const app = express()
app.use(cors());
app.use(express.json())
app.use(todoRoutes)

const PORT = process.env.PORT || 4000

app.get("/", (req, res) => res.send("Hello from server!"))

app.listen(PORT, () =>{
  connectToDb()
  console.log(`⚡Server is running here 👉 https://localhost:${PORT}`)

})
